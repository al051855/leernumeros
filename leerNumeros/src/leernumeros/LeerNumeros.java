/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leernumeros;

import java.util.Scanner;

/**
 *
 * @author juanm
 */
public class LeerNumeros {

    /*Pedir un número y leer n veces números, realizar la suma de los numeros, sacar el
    promedio y determinar cual número introducido es el mayor y cual el menor, y la
    distancia númerica entre ellos.*/
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        float suma = 0;
        float mayor = Integer.MIN_VALUE;
        float menor = Integer.MAX_VALUE;
        float distMayMen;
        float promedio;

        System.out.println("Ingresar n");
        int n = sc.nextInt();
        float[] numerosIngresados = new float[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Ingresar el " + (i + 1) + "° numero");
            numerosIngresados[i] = sc.nextFloat();
        }

        for (float numero : numerosIngresados) {
            suma += numero;
            
            if (numero > mayor){
                mayor = numero;
            }
            if(numero < menor){
                menor = numero;
            }
        }
        promedio = suma / n;
        distMayMen = mayor - menor;
        
        System.out.println("suma: " + suma);
        System.out.println("mayor: " + mayor);
        System.out.println("menor: " + menor);
        System.out.println("promedio: " + promedio);
        System.out.println("distancia del mayor al menor: " + distMayMen);

    }

}
